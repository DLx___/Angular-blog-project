import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-items',
  templateUrl: './post-list-items.component.html',
  styleUrls: ['./post-list-items.component.scss']
})
export class PostListItemsComponent implements OnInit {

  @Input() title: string;
  @Input() content: string;
  @Input() like: number;
  @Input() dislike: number;
  // tslint:disable-next-line:variable-name
  @Input() create_at: Date;

  constructor() {
  }

  ngOnInit() {
  }

  onLike() {
    this.like++;
  }

  onUnlike() {
    this.dislike++;
  }

}
