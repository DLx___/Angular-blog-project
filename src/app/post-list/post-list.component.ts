import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  posts = [
    {
      title: 'My first post',
      // tslint:disable-next-line:max-line-length
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis sapien enim, et suscipit quam ornare quis. Nam ac metus vitae turpis molestie sagittis non sit amet justo.',
      loveIts: 0,
      dontLove: 0,
      created_at: new Date(),
    },
    {
      title: 'My second post',
      // tslint:disable-next-line:max-line-length
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis sapien enim, et suscipit quam ornare quis. Nam ac metus vitae turpis molestie sagittis non sit amet justo.',
      loveIts: 0,
      dontLove: 0,
      created_at: new Date(),
    },
    {
      title: 'My third post',
      // tslint:disable-next-line:max-line-length
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque iaculis sapien enim, et suscipit quam ornare quis. Nam ac metus vitae turpis molestie sagittis non sit amet justo.',
      loveIts: 0,
      dontLove: 0,
      created_at: new Date(),
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
